package com.test.student.controller;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.test.student.entity.Student;
import com.test.student.service.StudentService;

@RestController
@RequestMapping("/students")
public class StudentController {

  @Autowired
  private StudentService studentService;

  @PostMapping
  public ResponseEntity<Student> addStudent(@RequestBody Student student) {
    return new ResponseEntity<>(studentService.addStudent(student), HttpStatus.CREATED);
  }

  @GetMapping("/{id}")
  public ResponseEntity<Student> getStudentById(@PathVariable Long id) {
    Optional<Student> student = studentService.getStudentById(id);
    return student.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @GetMapping("/name/{name}")
  public ResponseEntity<List<Student>> getStudentsByName(@PathVariable String name) {
    return new ResponseEntity<>(studentService.getStudentsByName(name), HttpStatus.OK);
  }

  @GetMapping("/class/{studentClass}")
  public ResponseEntity<List<Student>> getStudentsByClass(@PathVariable String studentClass) {
    return new ResponseEntity<>(studentService.getStudentsByClass(studentClass), HttpStatus.OK);
  }

  @PutMapping("/{id}")
  public ResponseEntity<Student> updateStudent(@PathVariable Long id,
      @RequestBody Student studentDetails) {
    return new ResponseEntity<>(studentService.updateStudent(id, studentDetails), HttpStatus.OK);
  }



}
